package ne.datashare;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ShareDataFragment extends CustomFragment implements SwipeRefreshLayout.OnRefreshListener {

    @InjectView(R.id.rcView)
    RecyclerView rcView;
    @InjectView(R.id.fab)
    FloatingActionButton fab;
    @InjectView(R.id.refresh)
    SwipeRefreshLayout refreshLayout;
    SharedPreferences prefs;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ShareDataFragment() {
        // Required empty public constructor
    }
    public static ShareDataFragment newInstance(String param1, String param2) {
        ShareDataFragment fragment = new ShareDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_share_data, container, false);
        ButterKnife.inject(this, v);
        prefs=getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), NewShareActivity.class));
            }
        });
        rcView.setHasFixedSize(true);
        rcView.setLayoutManager(new LinearLayoutManager(getActivity()));
        refreshLayout.setOnRefreshListener(this);
        loadData();
        return v;
    }

    private void loadData() {
        refreshLayout.setRefreshing(true);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("shares");
        Query query = myRef.orderByChild("Owner").equalTo(prefs.getString("key","NULL"));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                refreshLayout.setRefreshing(false);
                rcView.setAdapter(new SharedDataRCAdapter(dataSnapshot));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Found Children: "+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                refreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onFragmentResumed(FragmentActivity activity) {
        loadData();
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
