package ne.datashare;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pojo.DSData;

public class DataShareViewer extends AppCompatActivity {

    @InjectView(R.id.dataLayout)
    LinearLayout dataLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_share_viewer);
        ButterKnife.inject(this);
        if (getIntent() != null && getIntent().getData() != null) {
            Uri data = getIntent().getData();
            String code = data.getHost();
            startDecoding(code);
        }
    }

    /**
     * Get the 'Share' data and send it for processing
     * @param code stores the referral code
     */
    private void startDecoding(String code) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait");
        pd.setCancelable(false);
        pd.show();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("shares");
        myRef.child(code)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        pd.dismiss();
                        if(dataSnapshot!=null){
                            processFoundData((Map<String, Object>)dataSnapshot.getValue());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        pd.dismiss();
                        Toast.makeText(DataShareViewer.this, "No data found", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /**
     * Process the data that is found for the code and ask for the password
     * @param value stores the data permission object
     */
    private void processFoundData(final Map<String, Object> value) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_pass, null);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(false);
        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.txtPass);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                matchPasswordAndShowResult(userInput.getText().toString(), value);
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    /**
     * Match the password and retrieve the actual data
     * @param pass stores the password entered by user
     * @param value stores the actual user data
     */
    private void matchPasswordAndShowResult(String pass, final Map<String, Object> value) {
        if(pass.equals(value.get("Password"))){
            //1. get user data
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myRef = database.getReference().child("data");
            final ProgressDialog pd =new ProgressDialog(this);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
            myRef.child(value.get("Owner").toString()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    pd.cancel();
                    showData(value, (Map<String, Object>)dataSnapshot.getValue());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    pd.cancel();
                }
            });
        }else{
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Failed")
                    .setMessage("Password mismatch")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
        }
    }

    /**
     * Display the data on UI
     * @param access stores the data permission object
     * @param data stores the actual data
     */
    private void showData(Map<String, Object> access, Map<String, Object> data) {
        DSData contentData =new DSData(data);
        Log.e("Data", data+" ");
        boolean bName = (boolean) access.get("Name");
        boolean bAge = (boolean) access.get("Age");
        boolean bAddress = (boolean) access.get("Address");
        boolean bEmail = (boolean) access.get("Email");
        TextView txtLabel, txtData;
        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        float labelTextSize = convertSpToPixels(10, this);
        float dataTextSize = convertSpToPixels(14, this);
        if(bName){
            txtLabel = new TextView(this);
            txtLabel.setLayoutParams(linearParams);
            txtLabel.setText("Name");
            txtLabel.setTextSize(labelTextSize);
            dataLayout.addView(txtLabel);
            txtData = new TextView(this);
            txtData.setLayoutParams(linearParams);
            txtData.setText(contentData.getName());
            txtData.setTextSize(dataTextSize);
            dataLayout.addView(txtData);
        }
        if(bAddress){
            txtLabel = new TextView(this);
            txtLabel.setLayoutParams(linearParams);
            txtLabel.setText("Address");
            txtLabel.setTextSize(labelTextSize);
            dataLayout.addView(txtLabel);
            txtData = new TextView(this);
            txtData.setLayoutParams(linearParams);
            txtData.setText(contentData.getAddress());
            txtData.setTextSize(dataTextSize);
            dataLayout.addView(txtData);
        }
        if(bAge){
            txtLabel = new TextView(this);
            txtLabel.setLayoutParams(linearParams);
            txtLabel.setText("Age");
            txtLabel.setTextSize(labelTextSize);
            dataLayout.addView(txtLabel);
            txtData = new TextView(this);
            txtData.setLayoutParams(linearParams);
            txtData.setText(contentData.getAge());
            txtData.setTextSize(dataTextSize);
            dataLayout.addView(txtData);
        }
        if(bEmail){
            txtLabel = new TextView(this);
            txtLabel.setLayoutParams(linearParams);
            txtLabel.setText("Email");
            txtLabel.setTextSize(labelTextSize);
            dataLayout.addView(txtLabel);
            txtData = new TextView(this);
            txtData.setLayoutParams(linearParams);
            txtData.setText(contentData.getEmailId());
            txtData.setTextSize(dataTextSize);
            dataLayout.addView(txtData);
        }
    }

    /**
     * Convert input to SP unit
     * @param sp input in SP
     * @param context Current context
     * @return
     */
    public static float convertSpToPixels(float sp, Context context) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }
}
