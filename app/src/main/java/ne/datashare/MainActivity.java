package ne.datashare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pojo.DSUser;

public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.txtEmail)
    EditText txtEmail;
    @InjectView(R.id.txtPass)
    EditText txtPass;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("logged_in", false)) {
            startActivity(new Intent(this, Dashboard.class));
            finish();
        }
    }

    public void loginUser(View view) {
        String emailId = txtEmail.getText().toString();
        String password = txtPass.getText().toString();
        if (null == emailId || emailId.length() < 1) {
            Toast.makeText(this, "Please enter an email id", Toast.LENGTH_SHORT).show();
            return;
        }
        if (null == password || password.length() < 1) {
            Toast.makeText(this, "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }
        callLoginMethod(emailId, password);
    }

    private void callLoginMethod(final String emailId, final String password) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait");
        pd.setCancelable(false);
        pd.show();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("users");
        final DSUser user = new DSUser();
        user.setEmailid(emailId);
        user.setPassword(password);
        final DatabaseReference newUser = myRef.push();
        //See if email id exists
        Query query = myRef.orderByChild("emailid").equalTo(emailId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                pd.dismiss();
                if (dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        HashMap<String, String> dataMap = (HashMap<String, String>) data.getValue();
                        String email_ = dataMap.get("emailid");
                        if (email_.equals(emailId)) {
                            if (dataMap.get("password").equals(password)) {
                                Toast.makeText(MainActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                                prefs.edit().putBoolean("logged_in", true)
                                        .putString("emailid", emailId)
                                        .putString("key", data.getKey())
                                        .apply();
                                startActivity(new Intent(MainActivity.this, Dashboard.class));
                                finish();
                            } else {
                                Toast.makeText(MainActivity.this, "Wrong username/password", Toast.LENGTH_SHORT).show();
                            }
                        } else {

                        }
                    }
                    /*String storedPassword = dataSnapshot.child("password").toString();
                    Toast.makeText(MainActivity.this, "Stored Pass: "+storedPassword, Toast.LENGTH_SHORT).show();
                    if(storedPassword.equals(password)){
                        Toast.makeText(MainActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(MainActivity.this, "Password mismatch", Toast.LENGTH_SHORT).show();
                    }*/
                } else {
                    Toast.makeText(MainActivity.this, "No user found with this email id", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void openRegister(View view) {
        startActivity(new Intent(this, SignupActivity.class));
    }
}
