package ne.datashare;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by sumanta on 8/19/2017.
 */

class SharedDataRCAdapter extends RecyclerView.Adapter<SharedDataRCAdapter.SharedDataItem> {
    private List<DataSnapshot> dataSnapshots = new ArrayList<>();
    Context context;
    public SharedDataRCAdapter(DataSnapshot dataSnapshot) {
        for(DataSnapshot data : dataSnapshot.getChildren()){
            dataSnapshots.add(data);
        }
    }

    @Override
    public SharedDataItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shared_data_item, parent, false);
        context = parent.getContext();
        SharedDataItem dataItem =  new SharedDataItem(v);
        return dataItem;
    }

    @Override
    public void onBindViewHolder(SharedDataItem holder, int position) {
        Map<String, Object> data = (Map<String, Object>) dataSnapshots.get(position).getValue();
        boolean bAge = (boolean)data.get("Address");
        boolean bAddress = (boolean)data.get("Age");
        boolean bEmail = (boolean)data.get("Email");
        boolean bName = (boolean)data.get("Name");
        String password = ""+data.get("Password");
        holder.chkAddress.setChecked(bAddress);
        holder.chkAge.setChecked(bAge);
        holder.chkEmail.setChecked(bEmail);
        holder.chkName.setChecked(bName);
        holder.txtPassword.setText("Password: "+password);
    }

    @Override
    public int getItemCount() {
        return (int)dataSnapshots.size();
    }

    public class SharedDataItem extends RecyclerView.ViewHolder {
        @InjectView(R.id.chkName)
        CheckBox chkName;
        @InjectView(R.id.chkAge)
        CheckBox chkAge;
        @InjectView(R.id.chkAddress)
        CheckBox chkAddress;
        @InjectView(R.id.chkEmail)
        CheckBox chkEmail;
        @InjectView(R.id.btnShare)
        Button btnShare;
        @InjectView(R.id.txtPass)
        TextView txtPassword;
        public SharedDataItem(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DataSnapshot data = dataSnapshots.get(getLayoutPosition());
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT,"datashare://"+data.getKey());
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "DataShare");
                    context.startActivity(Intent.createChooser(shareIntent, "Share..."));
                }
            });
        }
    }
}
