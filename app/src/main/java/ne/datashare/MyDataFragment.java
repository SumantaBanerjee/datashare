package ne.datashare;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pojo.DSData;


public class MyDataFragment extends CustomFragment implements View.OnClickListener {

    @InjectView(R.id.txtName)
    EditText txtName;
    @InjectView(R.id.txtEmail)
    EditText txtEmail;
    @InjectView(R.id.txtAge)
    EditText txtAge;
    @InjectView(R.id.txtAddress)
    EditText txtAddress;
    @InjectView(R.id.btnSave)
    Button btnSave;
    SharedPreferences prefs;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MyDataFragment() {
        // Required empty public constructor
    }
    public static MyDataFragment newInstance(String param1, String param2) {
        MyDataFragment fragment = new MyDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_data, container, false);
        ButterKnife.inject(this, v);
        btnSave.setOnClickListener(this);
        retrieveData();
        return v;
    }

    private void retrieveData() {
        prefs=getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait");
        pd.setCancelable(false);
        pd.show();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("data");
        String key = prefs.getString("key","NULL");
        myRef.child(key)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        pd.dismiss();
                        DSData data = new DSData((Map<String, Object>) dataSnapshot.getValue());
                        populateData(data);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        pd.dismiss();
                    }
                });
    }

    private void populateData(DSData data) {
        txtName.setText(data.getName());
        txtAddress.setText(data.getAddress());
        txtAge.setText(data.getAge());
        txtEmail.setText(data.getEmailId());
    }

    @Override
    public void onClick(View view) {
        String name = txtName.getText().toString();
        String email = txtEmail.getText().toString();
        String age = txtAge.getText().toString();
        String address = txtAddress.getText().toString();
        if((null==name || name.length()<1) ||
                (null==email || email.length()<1) ||
                (null==age || age.length()<1) ||
                (null==address || address.length()<1)){
            Toast.makeText(getActivity(), "Please enter values", Toast.LENGTH_SHORT).show();
            return;
        }
        DSData data =new DSData(name, email, age, address);
        saveData(data);
    }

    private void saveData(final DSData data) {
        SharedPreferences prefs=getActivity().getSharedPreferences(getActivity().getPackageName(),Context.MODE_PRIVATE);
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait");
        pd.setCancelable(false);
        pd.show();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("data");
        //Get saved email id. If nothing is save go back to login screen
        String emailId = prefs.getString("emailid","");
        if(null == emailId || emailId.length()<1){
            prefs.edit().putBoolean("logged_in", false).apply();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
            return;
        }
        myRef.child(prefs.getString("key","NULL")).setValue(data, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                pd.cancel();
                if(null == databaseError){
                    Toast.makeText(getActivity(), "Saved Data", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
