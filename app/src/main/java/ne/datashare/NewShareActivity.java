package ne.datashare;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class NewShareActivity extends AppCompatActivity {

    @InjectView(R.id.chkName)
    CheckBox chkname;
    @InjectView(R.id.chkAge)
    CheckBox chkAge;
    @InjectView(R.id.chkAddress)
    CheckBox chkAddr;
    @InjectView(R.id.chkEmail)
    CheckBox chkMail;
    @InjectView(R.id.txtPass)
    EditText txtPassword;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_share);
        ButterKnife.inject(this);
        prefs=getSharedPreferences(getPackageName(),MODE_PRIVATE);
        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (Exception ex){}
    }

    public void createShare(View view) {
        Boolean bName = chkname.isChecked();
        Boolean bAge = chkAge.isChecked();
        Boolean bMail = chkMail.isChecked();
        Boolean bAddr = chkAddr.isChecked();
        String pass = txtPassword.getText().toString();
        Map<String, Object> dataShare = new HashMap<String, Object>();
        dataShare.put("Name", bName);
        dataShare.put("Age", bAge);
        dataShare.put("Address", bAddr);
        dataShare.put("Email", bMail);
        dataShare.put("Password", pass);
        dataShare.put("Owner", prefs.getString("key", "NULL"));
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait");
        pd.setCancelable(false);
        pd.show();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("shares");
        DatabaseReference dummy = myRef.push();
        myRef.child(dummy.getKey())
                .setValue(dataShare, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        pd.dismiss();
                        if(databaseError==null){
                            Toast.makeText(NewShareActivity.this, "Created new share", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(NewShareActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}

