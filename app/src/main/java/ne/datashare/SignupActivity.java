package ne.datashare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pojo.DSUser;

public class SignupActivity extends AppCompatActivity {

    @InjectView(R.id.txtEmail)
    EditText txtEmail;
    @InjectView(R.id.txtPass)
    EditText txtPass;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.inject(this);
        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (Exception ex){}
        prefs=getSharedPreferences(getPackageName(),MODE_PRIVATE);
    }

    public void signupUser(View view) {
        String emailId = txtEmail.getText().toString();
        String password = txtPass.getText().toString();
        if (null == emailId || emailId.length() < 1) {
            Toast.makeText(this, "Please enter an email id", Toast.LENGTH_SHORT).show();
            return;
        }
        if (null == password || password.length() < 1) {
            Toast.makeText(this, "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }
        callSignup(emailId, password);
    }

    private void callSignup(final String emailId, String password) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please wait");
        pd.setCancelable(false);
        pd.show();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("users");
        final DSUser user = new DSUser();
        user.setEmailid(emailId);
        user.setPassword(password);
        final DatabaseReference newUser = myRef.push();
        //See if email id exists
        Query query = myRef.orderByChild("emailid").equalTo(emailId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                pd.dismiss();
                if(dataSnapshot.getChildrenCount()!=0){
                    Toast.makeText(SignupActivity.this, "Email already exists", Toast.LENGTH_SHORT).show();
                }else{
                    myRef.child(newUser.getKey()).setValue(user, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            pd.dismiss();
                            if(databaseError==null){
                                Toast.makeText(SignupActivity.this, "Signed Up", Toast.LENGTH_SHORT).show();
                                prefs.edit().putBoolean("logged_in", true)
                                        .putString("emailid", emailId)
                                        .putString("key", newUser.getKey())
                                        .apply();
                                startActivity(new Intent(SignupActivity.this, Dashboard.class));
                                finish();
                            }else{
                                Toast.makeText(SignupActivity.this, "Error: "+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                pd.dismiss();
                Toast.makeText(SignupActivity.this, "Error: "+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        //end
        /**/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
