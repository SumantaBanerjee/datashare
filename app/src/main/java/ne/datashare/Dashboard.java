package ne.datashare;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Dashboard extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @InjectView(R.id.tabLayout)
    TabLayout tabLayout;
    @InjectView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.inject(this);
        viewPager.setAdapter(new CustomViewPagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        try {
            CustomViewPagerAdapter adapter = (CustomViewPagerAdapter) viewPager.getAdapter();
            CustomFragment currentFragment = (CustomFragment) adapter.getItem(position);
            currentFragment.onFragmentResumed(this);
        }catch (Exception ex){
            //Casting exception
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
