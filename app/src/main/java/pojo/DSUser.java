package pojo;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by sumanta on 8/14/2017.
 */
@IgnoreExtraProperties
public class DSUser {
    String emailid;
    String password;

    public DSUser(){

    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DSUser(String emailid, String password) {
        this.emailid = emailid;
        this.password = password;
    }
}
