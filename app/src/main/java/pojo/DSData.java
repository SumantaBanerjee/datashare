package pojo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sumanta on 8/17/2017.
 */

public class DSData {
    private String name;
    private String emailId;
    private String age;
    private String address;

    public DSData(String name, String emailId, String age, String address) {
        this.name = name;
        this.emailId = emailId;
        this.age = age;
        this.address = address;
    }

    public DSData(Map<String, Object> map){
        try{
            this.name=map.get("name").toString();
            this.emailId=map.get("emailId").toString();
            this.age=map.get("age").toString();
            this.address=map.get("address").toString();
        }catch (Exception ex){}
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
